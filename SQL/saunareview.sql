CREATE DATABASE saunareview DEFAULT CHARACTER SET utf8;
use saunareview;
create TABLE user(
id int PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE not null,
name varchar(255) not null,
birth_date date not null,
password varchar(255) not null,
is_admin boolean DEFAULT false not null
);

CREATE TABLE sauna(
id int PRIMARY KEY AUTO_INCREMENT,
name varchar(255) not null,
location varchar(255) not null,
price int not null,
sauna_temp int not null,
water_temp int not null
);

create table comment(
id int PRIMARY KEY AUTO_INCREMENT,
user_id int not null,
sauna_id int not null,
post_name varchar(255) not null,
create_date datetime not null,
rate int not null,
body text not null,
FOREIGN KEY(user_id) REFERENCES user(id),
FOREIGN KEY(sauna_id) REFERENCES sauna(id)
);
