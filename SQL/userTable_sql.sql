CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

CREATE table
user(
id serial PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date date NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean default false NOT NULL,
create_date datetime NOT NULL,
update_date datetime NOT NULL
);

insert into
user(
login_id,name,birth_date,password,is_admin,create_date,update_date
)
values(
'admin',
'管理者',
'1986-11-11',
'password',
true,
now(),
now()
);
