package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

public class UserDao {
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  /**
   * 全てのユーザ情報を取得する
   *
   * @return
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM user WHERE is_admin = false";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  public void insert(String loginId, String password, String userName, String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // insert文を準備
      String sql =
          "insert into user(login_id,name,birth_date,password,create_date,update_date) values(?,?,?,?,now(),now());";

      // insertを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, userName);
      pStmt.setString(3, birthDate);
      pStmt.setString(4, password);
      int result = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public User findById(int id) {
    Connection conn = null;
    User user = new User();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // SELECT文を準備
      String sql =
          "select login_id,name,birth_date,create_date,update_date from user where id = ?;";

      // SELECTを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を Userインスタンスに追加
      if (!rs.next()) {
        return null;
      }
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      user.setId(id);
      user.setLoginId(loginId);
      user.setName(name);
      user.setBirthDate(birthDate);
      user.setCreateDate(createDate);
      user.setUpdateDate(updateDate);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return user;
  }

  public void updateAll(int id, String password, String userName, String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // update文を準備
      String sql =
          "update user set name =?,birth_date =?,password = ?,update_date = now() where id = ?;";

      // updateを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userName);
      pStmt.setString(2, birthDate);
      pStmt.setString(3, password);
      pStmt.setInt(4, id);
      int result = pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void updateExceptPass(int id, String userName, String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // passwordを除いた、更新をする際のupdate文
      String sql = "update user set name = ?,birth_date = ?,update_date = now() where id = ?;";


      // updateを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userName);
      pStmt.setString(2, birthDate);
      pStmt.setInt(3, id);
      int result = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void deleteUser(int id) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // Delete文を準備
      String sql = "DELETE FROM user WHERE id = ?;";

      // Deleteを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      // 実行し、結果表を取得
      int rs = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public int confirmLoginId(String loginId) {
    Connection conn = null;
    int id = 0;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // confoirm文を準備
      String sql = "select * FROM user WHERE login_id = ?;";

      // Deleteを準備
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);

      // 実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();
      if (rs.next()) {
        id = 1;
        return id;
      }


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return id;

  }

  public List<User> search(String loginId, String userName, String dateStart, String dateEnd) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "select * from user where is_admin = false ";

      StringBuilder sb = new StringBuilder(sql);

      List<String> list = new ArrayList<String>();
      userName = "%" + userName + "%";

      if (!loginId.equals("")) {
        sb.append(" and login_id = ?");
        list.add(loginId);
      }
      if (!userName.equals("")) {
        sb.append(" and name like ?");
        list.add(userName);
      }
      if (!dateStart.equals("")) {
        sb.append(" and birth_date >= ?");
        list.add(dateStart);
      }
      if (!dateEnd.equals("")) {
        sb.append(" and birth_date <= ?");
        list.add(dateEnd);
      }

      PreparedStatement pStmt = conn.prepareStatement(sb.toString());
      for (int i = 0; i < list.size(); i++) {
        pStmt.setString(i + 1, list.get(i));
      }
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容をUserインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String _loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, _loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

}

