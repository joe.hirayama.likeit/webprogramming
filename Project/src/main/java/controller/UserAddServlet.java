package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncoder;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // セッション情報が切れていたら、ログイン画面に強制遷移
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");
    if (user == null) {
      // リダイレクト
      response.sendRedirect("UserListServlet");
      return;
    }

    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");
    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");
    String passwordConfirm = request.getParameter("password-confirm");
    String userName = request.getParameter("user-name");
    String birthDate = request.getParameter("birth-date");

    UserDao userDao = new UserDao();
    int confirmLoginId = userDao.confirmLoginId(loginId);

    PasswordEncoder p = new PasswordEncoder();
    String encodedPassword = p.encodePassword(password);


    // loginIdやuserNameなどが未記入であったり、passwordが確認用passwordと合致しなかった場合
    if (loginId.equals("") || userName.equals("") || birthDate.equals("") || password.equals("")
        || passwordConfirm.equals("") || !password.equals(passwordConfirm) || confirmLoginId == 1) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "エラーメッセージ");
      // 入力した情報を画面に表示するため、リクエストに値をセット
      request.setAttribute("loginId", loginId);
      request.setAttribute("userName", userName);
      request.setAttribute("birthDate", birthDate);

      // userAdd.jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;

    } else {
      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      userDao.insert(loginId, encodedPassword, userName, birthDate);
      response.sendRedirect("UserListServlet");
      return;
    }
  }
}
