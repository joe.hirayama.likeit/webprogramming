package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      User u = (User) session.getAttribute("userInfo");
      if (u == null) {
        // リダイレクトのコードを書く
        response.sendRedirect("LoginServlet");
        return;
      }
      // ユーザ一覧情報を取得
      UserDao userDao = new UserDao();
      List<User> userList = userDao.findAll();
      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("userList", userList);

      // // 最新のログインユーザー情報を取得(更新した時に右上の名前が変わるよう)
      // User user = userDao.findById(u.getId());
      // // リクエストスコープにユーザ一覧情報をセット
      // request.setAttribute("userName", user);



      // ユーザ一覧のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("user-loginid");
      String userName = request.getParameter("user-name");
      String dateStart = request.getParameter("date-start");
      String dateEnd = request.getParameter("date-end");

      // searchメソッドを実行
      UserDao userDao = new UserDao();
      List<User> userList = userDao.search(loginId, userName, dateStart, dateEnd);


      // リクエストスコープにsearchしたユーザ一覧情報をセット
      request.setAttribute("userList", userList);
      request.setAttribute("dateStart", dateStart);
      request.setAttribute("dateEnd", dateEnd);
      request.setAttribute("loginId", loginId);
      request.setAttribute("userName", userName);


      // ユーザ一覧のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
	}

}
